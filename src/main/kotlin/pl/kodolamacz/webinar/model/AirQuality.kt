package pl.kodolamacz.webinar.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate

@Document
data class AirQuality(val city: String, val quality: Int, val date: LocalDate) {

    @Id
    lateinit var id: String
}
