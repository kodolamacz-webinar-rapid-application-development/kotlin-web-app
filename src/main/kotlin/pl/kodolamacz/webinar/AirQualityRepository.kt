package pl.kodolamacz.webinar

import pl.kodolamacz.webinar.model.AirQuality
import org.springframework.data.mongodb.repository.MongoRepository

interface AirQualityRepository : MongoRepository<AirQuality, String> {

    fun findAllByCity(city: String) : List<AirQuality>
}
