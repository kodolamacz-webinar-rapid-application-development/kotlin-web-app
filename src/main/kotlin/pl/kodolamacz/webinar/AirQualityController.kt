package pl.kodolamacz.webinar

import pl.kodolamacz.webinar.model.AirQuality
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("v1/air_quality")
class AirQualityController(val repository: AirQualityRepository) {

    @GetMapping
    fun hello() = "Hello"

    @GetMapping("/{id}")
    fun getAirQuality(@PathVariable id: String) = repository.findById(id)

    @PostMapping
    fun addAirQuality(@RequestBody airQuality: AirQuality) = repository.save(airQuality)

    @DeleteMapping("/{id}")
    fun deleteAirQuality(@PathVariable id: String) = repository.deleteById(id)

    @GetMapping("/cities/{city}")
    fun getAllAirQualityByCity(@PathVariable city: String) = repository.findAllByCity(city)
}
